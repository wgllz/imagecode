package com.bluemobi.www.exception;

public class UploadImageExcetion extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5103968698900580611L;

	public UploadImageExcetion() {
		super();
	}

	public UploadImageExcetion(String message, Throwable cause) {
		super(message, cause);
	}

	public UploadImageExcetion(String message) {
		super(message);
	}

	public UploadImageExcetion(Throwable cause) {
		super(cause);
	}
}

package com.bluemobi.www.exception;

public class PushException extends RuntimeException
{

    /**  */
    private static final long serialVersionUID = 2097344268000486329L;

    public PushException()
    {
        super();
    }

    public PushException( String message,Throwable cause )
    {
        super( message, cause );
    }

    public PushException( String message )
    {
        super( message );
    }

    public PushException( Throwable cause )
    {
        super( cause );
    }
}

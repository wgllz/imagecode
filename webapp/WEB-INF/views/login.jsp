<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>登录</title>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common/common.css" />
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/common/login.css" />
		<script type="text/javascript" src="<%=request.getContextPath()%>/scripts/jquery.min.js"></script>
		<script type="text/javascript">
			var global_ctxPath = '<%=request.getContextPath()%>'; 
		</script>
		<script type="text/javascript">
			
			function changeValidate(){
				$("#validateCode").attr("src", global_ctxPath + "/captcha?abc=" + Math.random());
			}
			
		</script>
	</head>
	<body>

		<div class="container-fluid main" >
			<form method="post" action="${ pageContext.request.contextPath}/login" class="form-signin" id="j_loginform">
				<h2 class="form-signin-heading">登录</h2>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="2"><input type="text" path="username" id="j_username" class="input-block-level" placeholder="用户名" tabindex="1" onblur="this.value=newtrim(this.value);"/></td>
					</tr>
					<tr>
						<td colspan="2"><input type="password" class="input-block-level" path="password" id="j_password" tabindex="2" placeholder="密码" /></td>
					</tr>
					<tr>
						<td><input type="text" maxlength="4" class="input-block-level" path="captcha" id="j_captcha" tabindex="3" placeholder="验证码" onblur="this.value=newtrim(this.value);"/></td>
						<td align="center">
						<a style="display:block;margin-bottom:15px;" href="javascript:void(0)" onclick="changeValidate();"><img height="32" align="middle" alt="验证码" id="validateCode" src="${ pageContext.request.contextPath}/captcha" title="换一张" /></a>
						<a style="display:block;margin-bottom:15px;" href="javascript:void(0)" onclick="changeValidate();">换一张</a></td>
					</tr>
					<tr>
						<td colspan="2"><button type="button" class="btn btn-large btn-primary" tabindex="4" onclick="submitForm();">登录</button></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="navbar navbar-fixed-bottom">©2013 XXX 版权所有</div>
	</body>
</html>

